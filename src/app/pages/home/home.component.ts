import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private title = 'treasure-map';
  private fileText;

  constructor() {
  }

  ngOnInit() {
  }

  onFileTextUpdated(fileText: string) {
    this.fileText = fileText;
  }
}
