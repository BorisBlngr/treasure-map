import {Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {TreasureMap} from "../../shared/models/treasure-map";
import {TreasureMapService} from "../../shared/services/treasure-map.service";
import * as FileSaver from "file-saver";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() fileText: string;
  private output: string;
  private treasureMap: TreasureMap;

  constructor(private treasureMapService: TreasureMapService) {
  }

  ngOnInit() {
    this.treasureMap = this.treasureMapService.getTreasureMap(this.fileText);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.treasureMap = this.treasureMapService.getTreasureMap(this.fileText);
  }

  runMovements() {
    this.treasureMap.moveAdventurers();
    this.output = this.treasureMapService.toFileText(this.treasureMap);
  }

  download() {
    let blob = new Blob([this.output], {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(blob, "Treasure map.txt");
  }
}
