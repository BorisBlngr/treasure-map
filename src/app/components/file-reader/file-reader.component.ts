import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-file-reader',
  templateUrl: './file-reader.component.html',
  styleUrls: ['./file-reader.component.scss']
})
export class FileReaderComponent implements OnInit {

  @Output() fileTextUpdated = new EventEmitter<string>();

  private fileText: string;

  constructor() {
  }

  ngOnInit() {
  }

  setFileText(fileText: string) {
    this.fileText = fileText;
    this.fileTextUpdated.emit(this.fileText);
  }

  fileUpload(event) {
    let reader = new FileReader();
    let me = this;
    reader.readAsText(event.srcElement.files[0]);
    reader.onload = function () {
      me.setFileText(reader.result.toString());
    }
  }
}
