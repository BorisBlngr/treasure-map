export enum MapSquareMiscellaneous {
  TREASURE = 'T',
  ADVENTURER = 'A',
  MAP = 'C'
}
