import {MapSquare} from "./map-square";
import {Adventurer} from "./adventurer";
import {Movement} from "./enums/movement.enum";
import {Direction} from "./enums/direction.enum";

export class TreasureMap {
  private _turn: number;
  private _height: number;
  private _width: number;
  private _map: MapSquare[][] = [];
  private _adventurers: Adventurer[] = [];

  constructor(height: number, width: number) {
    this._height = height;
    this._width = width;

    this.initializeMap();
  }

  private initializeMap() {
    this._map = new Array(this._height);
    for (let indexVertical = 0; indexVertical < this._height; indexVertical++) {
      this._map[indexVertical] = new Array(this._width);

      for (let indexHorizontal = 0; indexHorizontal < this._width; indexHorizontal++)
        this._map[indexVertical][indexHorizontal] = new MapSquare();
    }
  }

  get adventurers(): Adventurer[] {
    return this._adventurers;
  }

  get height(): number {
    return this._height;
  }

  get width(): number {
    return this._width;
  }

  get map(): MapSquare[][] {
    return this._map;
  }

  get turn(): number {
    return this._turn;
  }

  positionAdventurers() {
    this._adventurers.forEach(adventurer => this._map[adventurer.positionVertical][adventurer.positionHorizontal].addAdventurer(adventurer));
  }

  addSquare(square: MapSquare, horizontalPosition: number, verticalPosition: number) {
    this._map[verticalPosition][horizontalPosition] = square;
  }

  addAdventurer(newAdventurer: Adventurer) {
    if (!this._adventurers.some(adventurer => adventurer.isAtSamePosition(newAdventurer))
      && newAdventurer.positionHorizontal < this._width
      && newAdventurer.positionVertical < this._height)
      this._adventurers.push(newAdventurer)
  }

  moveAdventurers() {
    let turnMax = Math.max.apply(Math, this._adventurers.map(adventurer => adventurer.movements.length));

    for (let turn = 0; turn < turnMax; turn++) {
      this._turn = turn;
      this._adventurers.forEach(adventurer => this.moveAdventurer(adventurer, turn));
    }
  }

  private moveAdventurer(adventurer: Adventurer, turn: number) {
    let nextMove: Movement = adventurer.movements[turn];

    if (!nextMove)
      return;

    switch (nextMove) {
      case Movement.RIGHT: {
        adventurer.turnRight();
        break;
      }
      case Movement.LEFT: {
        adventurer.turnLeft();
        break;
      }
      case Movement.MOVE: {
        this.moveForward(adventurer);
        break;
      }
    }
  }

  private moveForward(adventurer: Adventurer) {
    switch (adventurer.direction) {
      case Direction.NORTH  : {
        if (adventurer.positionVertical === 0) break;
        if (!this._map[adventurer.positionVertical - 1][adventurer.positionHorizontal].addAdventurer(adventurer)) break;
        this._map[adventurer.positionVertical][adventurer.positionHorizontal].removeAdventurer();
        adventurer.positionVertical -= 1;
        break;
      }
      case Direction.SOUTH  : {
        if (adventurer.positionVertical === this._height - 1) break;
        if (!this._map[adventurer.positionVertical + 1][adventurer.positionHorizontal].addAdventurer(adventurer)) break;
        this._map[adventurer.positionVertical][adventurer.positionHorizontal].removeAdventurer();
        adventurer.positionVertical += 1;
        break;
      }
      case Direction.EST  : {
        if (adventurer.positionHorizontal === this._width - 1) break;
        if (!this._map[adventurer.positionVertical][adventurer.positionHorizontal + 1].addAdventurer(adventurer)) break;
        this._map[adventurer.positionVertical][adventurer.positionHorizontal].removeAdventurer();
        adventurer.positionHorizontal += 1;
        break;
      }
      case Direction.WEST  : {
        if (adventurer.positionHorizontal === 0) break;
        if (!this._map[adventurer.positionVertical][adventurer.positionHorizontal - 1].addAdventurer(adventurer)) break;
        this._map[adventurer.positionVertical][adventurer.positionHorizontal].removeAdventurer();
        adventurer.positionHorizontal -= 1;
        break;
      }
    }
  }
}
