export class Treasures {
  get quantity(): number {
    return this._quantity;
  }

  removeOne(): boolean {
    if (!this.hasTreasures())
      return false;
    this._quantity -= 1;
    return true
  }

  hasTreasures(): boolean {
    return this._quantity > 0;
  }

  private _quantity: number;

  constructor(quantity: number = 0) {
    this._quantity = quantity;
  }


}
