import {Direction} from "./enums/direction.enum";
import {Movement} from "./enums/movement.enum";

const directions = Object.keys(Direction.valueOf()).map(value => Direction[value]);

export class Adventurer {
  set positionVertical(value: number) {
    this._positionVertical = value;
  }

  private _name: string;
  private _positionHorizontal: number;
  private _positionVertical: number;
  private _direction: Direction;
  private _movements: Movement[];
  private _treasures: number = 0;


  constructor(name: string, positionHorizontal: number, positionVertical: number, direction, movements: any[]) {
    this._name = name;
    this._positionHorizontal = positionHorizontal;
    this._positionVertical = positionVertical;
    this._direction = direction;
    this._movements = movements;
  }

  set positionHorizontal(value: number) {
    this._positionHorizontal = value;
  }

  get name(): string {
    return this._name;
  }

  get treasures(): number {
    return this._treasures;
  }

  get positionHorizontal(): number {
    return this._positionHorizontal;
  }

  get positionVertical(): number {
    return this._positionVertical;
  }

  get direction(): Direction {
    return this._direction;
  }

  get movements(): Movement[] {
    return this._movements;
  }

  addTreasure() {
    this._treasures += 1;
  }

  turnRight() {
    let index = directions.findIndex(direction => direction === this._direction)
    if (index === directions.length - 1)
      this._direction = directions[0];
    else
      this._direction = directions[index + 1];
  }

  turnLeft() {
    let index = directions.findIndex(direction => direction === this._direction)
    if (index === 0)
      this._direction = directions[directions.length - 1];
    else
      this._direction = directions[index - 1];
  }


  isAtSamePosition(adventurer: Adventurer): boolean {
    return adventurer.positionHorizontal === this._positionHorizontal && adventurer.positionVertical === this._positionVertical;
  }
}
