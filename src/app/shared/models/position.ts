export class Position {
  private verticalPosition: number;
  private horizontalPosition: number;


  constructor(verticalPosition: number, horizontalPosition: number) {
    this.verticalPosition = verticalPosition;
    this.horizontalPosition = horizontalPosition;
  }
}
