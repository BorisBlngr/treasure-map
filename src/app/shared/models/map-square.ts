import {MapSquareType} from "./enums/map-square-type.enum";
import {Treasures} from "./treasures";
import {Adventurer} from "./adventurer";

export class MapSquare {

  private _type: MapSquareType;
  private _treasures: Treasures;
  private _adventurer: Adventurer;

  constructor(type: MapSquareType = MapSquareType.LOWLAND) {
    this._type = type;
    this._treasures = new Treasures();
  }

  get treasures(): Treasures {
    return this._treasures;
  }

  get type(): MapSquareType {
    return this._type;
  }

  get adventurer(): Adventurer {
    return this._adventurer;
  }

  removeAdventurer() {
    this._adventurer = null;
  }

  addTreasures(treasures: Treasures) {
    if (this._type === MapSquareType.MOUNTAIN)
      return;
    this._treasures = treasures;
  }

  addAdventurer(adventurer: Adventurer): boolean {
    if (this._adventurer)
      return false;
    if (this._type === MapSquareType.MOUNTAIN)
      return false;
    this._adventurer = adventurer;

    if (this._treasures.removeOne()) {
      this._adventurer.addTreasure();
    }
    return true;
  }
}
