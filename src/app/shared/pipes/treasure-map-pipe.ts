import {Pipe, PipeTransform} from "@angular/core";
import {MapSquare} from "../models/map-square";
import {MapSquareMiscellaneous} from "../models/enums/map-square-miscellaneous.enum";
import {MapSquareType} from "../models/enums/map-square-type.enum";

@Pipe({name: 'treasureMap'})
export class TreasureMapPipe implements PipeTransform {
  transform(mapSquare: MapSquare, trigger: number): any {
    if (mapSquare.treasures.quantity > 0)
      return MapSquareMiscellaneous.TREASURE + `(${mapSquare.treasures.quantity})`;

    else if (mapSquare.adventurer)
      return MapSquareMiscellaneous.ADVENTURER + `(${mapSquare.adventurer.name})`;

    else if (mapSquare.type === MapSquareType.LOWLAND)
      return '.';

    else
      return mapSquare.type;
  }
}
