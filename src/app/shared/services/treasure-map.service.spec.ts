import {TestBed} from '@angular/core/testing';

import {TreasureMapService} from './treasure-map.service';

describe('TreasureMapService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TreasureMapService = TestBed.get(TreasureMapService);
    expect(service).toBeTruthy();
  });
});
