import {Injectable} from '@angular/core';
import {TreasureMap} from "../models/treasure-map";
import {MapSquareType} from "../models/enums/map-square-type.enum";
import {MapSquare} from "../models/map-square";
import {MapSquareMiscellaneous} from "../models/enums/map-square-miscellaneous.enum";
import {Treasures} from "../models/treasures";
import {Adventurer} from "../models/adventurer";

const LINE_SEPARATOR: string = '\n';
const WORD_SEPARATOR: string = '-';

const MAP_INFO_HEIGHT: number = 2;
const MAP_INFO_WIDTH: number = 1;

const TREASURE_HORIZONTAL: number = 1;
const TREASURE_VERTICAL: number = 2;
const TREASURE_QUANTITY: number = 3;


const ADVENTURER_NAME: number = 1;
const ADVENTURER_HORIZONTAL: number = 2;
const ADVENTURER_VERTICAL: number = 3;
const ADVENTURER_DIRECTION: number = 4;
const ADVENTURER_MOVEMENTS: number = 5;

const COMMENT_ADVENTURER: string = '# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}\r\n';
const COMMENT_TREASURES: string = '# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}\r\n';

@Injectable({
  providedIn: 'root'
})
export class TreasureMapService {

  constructor() {
  }

  getTreasureMap(fileText: string): TreasureMap {
    let lines: string[] = fileText.split(LINE_SEPARATOR);
    let mapInfo: string[] = lines.find(line => line.startsWith(MapSquareMiscellaneous.MAP)).split(WORD_SEPARATOR);
    let treasureMap: TreasureMap = new TreasureMap(Number(mapInfo[MAP_INFO_HEIGHT]), Number(mapInfo[MAP_INFO_WIDTH]));

    this.addMountains(treasureMap, lines);
    this.addTreasure(treasureMap, lines);
    this.addAdventurer(treasureMap, lines);
    treasureMap.positionAdventurers();

    return treasureMap;
  }

  private addMountains(treasureMap: TreasureMap, lines: string[]) {
    lines
      .filter(line => line.startsWith(MapSquareType.MOUNTAIN))
      .forEach(line => {
        let lineInfo: string[] = line.split(WORD_SEPARATOR);
        let square: MapSquare = new MapSquare(MapSquareType.MOUNTAIN);
        treasureMap.addSquare(square, Number(lineInfo[TREASURE_HORIZONTAL]), Number(lineInfo[TREASURE_VERTICAL]));
      })
  }

  private addTreasure(treasureMap: TreasureMap, lines: string[]) {
    lines
      .filter(line => line.startsWith(MapSquareMiscellaneous.TREASURE))
      .forEach(line => {
        let lineInfo: string[] = line.split(WORD_SEPARATOR);
        let square: MapSquare = new MapSquare();
        square.addTreasures(new Treasures(Number(lineInfo[TREASURE_QUANTITY])));
        treasureMap.addSquare(square, Number(lineInfo[TREASURE_HORIZONTAL]), Number(lineInfo[TREASURE_VERTICAL]));
      })
  }

  private addAdventurer(treasureMap: TreasureMap, lines: string[]) {
    lines
      .filter(line => line.startsWith(MapSquareMiscellaneous.ADVENTURER))
      .forEach(line => {
        let lineInfo: string[] = line.split(WORD_SEPARATOR);
        let adventurer = new Adventurer(
          lineInfo[ADVENTURER_NAME].trim(),
          Number(lineInfo[ADVENTURER_HORIZONTAL]),
          Number(lineInfo[ADVENTURER_VERTICAL]),
          lineInfo[ADVENTURER_DIRECTION].trim(),
          lineInfo[ADVENTURER_MOVEMENTS].trim().split('')
        );
        treasureMap.addAdventurer(adventurer);
      })
  }

  toFileText(treasureMap: TreasureMap,): string {
    let mapInfo = `${MapSquareMiscellaneous.MAP} - ${treasureMap.width} - ${treasureMap.height}\r\n`;
    let treasures: string = COMMENT_TREASURES;
    let mountains: string = '';
    let adventurers: string = COMMENT_ADVENTURER;

    treasureMap.map.forEach((verticalArray, verticalIndex) => {
      verticalArray.forEach((mapSquare, horizontalIndex) => {
        if (mapSquare.type === MapSquareType.MOUNTAIN)
          mountains += `${MapSquareType.MOUNTAIN} - ${horizontalIndex} - ${verticalIndex}\r\n`;

        if (mapSquare.treasures.hasTreasures())
          treasures += `${MapSquareMiscellaneous.TREASURE} - ${horizontalIndex} - ${verticalIndex} - ${mapSquare.treasures.quantity}\r\n`;
      })
    });

    treasureMap.adventurers.forEach(adventurer => {
      adventurers += `${MapSquareMiscellaneous.ADVENTURER} - ${adventurer.name} - ${adventurer.positionHorizontal} - ` +
        `${adventurer.positionVertical} - ${adventurer.direction} - ${adventurer.treasures}\r\n`;
    });

    return mapInfo + mountains + treasures + adventurers;
  }
}
