import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {FileReaderComponent} from './components/file-reader/file-reader.component';
import {TreasureMapService} from "./shared/services/treasure-map.service";
import {MapComponent} from './components/map/map.component';
import {TreasureMapPipe} from "./shared/pipes/treasure-map-pipe";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatButtonModule} from "@angular/material";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FileReaderComponent,
    MapComponent,
    TreasureMapPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AppRoutingModule
  ],
  providers: [TreasureMapService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
